React = require("react")
bv = require("backbone_views")


class Comment extends React.Component
  render: ->
    return (
      <div className="comment">
        <h2 className="commentAuthor">
          {this.props.author}
        </h2>
        {this.props.children}
      </div>
    )

class CommentList extends React.Component
  render: ->
    commentNodes = this.props.data.map (comment) ->
      return (
        <Comment author={comment.author}>
          {comment.text}
        </Comment>
      )
    return (
      <div className="commentList">
        {commentNodes}
      </div>
    )


class CommentForm extends React.Component
  handleSubmit: (e) ->
    e.preventDefault()
    console.log @, @refs
    author = React.findDOMNode(this.refs.author).value.trim()
    text = React.findDOMNode(this.refs.text).value.trim()
    if not text or not author
      return

    this.props.onSubmit({author: author, text: text})

    React.findDOMNode(this.refs.author).value = ''
    React.findDOMNode(this.refs.text).value = ''
    return

  render: ->
    return (
      <form className="commentForm" onSubmit={@handleSubmit.bind(this)}>
        <input type="text" placeholder="Your name" ref="author" />
        <input type="text" placeholder="Say something..." ref="text" />
        <input type="submit" value="Post" />
      </form>
    )


class CommentBox extends React.Component

  constructor: (options) ->
    super()
    @state = options

  handleSubmit: (data) ->
    this.setState({data: this.state.data.concat([data])})

  render: ->
    return (
      <div className="commentBox">
        <h1>Comments</h1>
        <CommentList data={@state.data} />
        <CommentForm onSubmit={@handleSubmit.bind(@)} />
      </div>
    )


class CommentBoxView extends bv.MixinView
  mixins: [bv.Reactor]
  component: CommentBox

  getContext: (context) ->
    context.data = @collection.toJSON()
    return context


module.exports = CommentBoxView
